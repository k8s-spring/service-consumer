FROM alpine:latest

RUN apk add --no-cache --update openjdk11-jre

RUN addgroup -S spring && adduser -S spring -G spring

USER spring:spring

WORKDIR /usr/app

COPY build/libs/*.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "app.jar"]
