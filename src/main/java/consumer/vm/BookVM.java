package consumer.vm;

import java.util.Date;

/**
 * Book
 */
public class BookVM {

    private Integer id;
    private String title;
    private Double price;
    private String isbn;
    private Date publishDate;

    public BookVM(Integer id, String title, Double price, String isbn) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.isbn = isbn;

        this.publishDate = new Date();
    }

    public BookVM() {

    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(final Double price) {
        this.price = price;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(final String isbn) {
        this.isbn = isbn;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(final Date publishDate) {
        this.publishDate = publishDate;
    }

    public void setId(final Integer id) {
        this.id = id;
    }
}
