package consumer.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import consumer.vm.BookVM;

/**
 * BookService
 */
@FeignClient(name = "${service.provider}", url = "http://${service.provider}")
public interface BookService {

    @GetMapping("/books")
    List<BookVM> books();
}
