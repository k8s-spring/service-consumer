package consumer.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import consumer.config.BookConfig;
import consumer.service.BookService;
import consumer.vm.BookVM;

@RestController
public class ConsumerController {

    @Autowired
    private BookConfig config;

    @Autowired
    private BookService bookService;

    @GetMapping("/hello")
    public List<String> hello() {
        return Arrays.asList("Hello", "World", "from", "book", "service");
    }

    @GetMapping("/books")
    public List<BookVM> books() {
        return bookService.books();
    }

    @GetMapping("/shop")
    public String prop() {
        return config.getMobile();
    }
}
